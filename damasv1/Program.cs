﻿using System;

namespace damasv1
{
    class Program
    {
        static void Main(string[] args)
        {
            Tablero tablero = new Tablero();

            tablero.crearTablero();
            int vueltas = 0;
            int jugador = 1;

            while (true) 
            {
                if((vueltas%2) == 0) 
                {
                    jugador = 1;
                }
                else 
                {
                    jugador = 2;
                }
                
                for (int i = 0; i < 8; i++)
                {
                    Console.Write("-----------------");
                    Console.WriteLine();
                    for (int j = 0; j < 8; j++)
                    {
                        Console.Write("|");
                        Console.Write(tablero.TableroJuego[i, j].Pieza);

                    }
                    Console.Write("|");
                    Console.WriteLine();
                }      
                Console.Write("-----------------");
                Console.WriteLine();
                Console.WriteLine("Elegir fila de la pieza a mover (0-7)");
                int guardarFila = int.Parse(Console.ReadLine());
                Console.WriteLine("Elegir Columna de la pieza a mover (0-7)");
                int guardarColumna = int.Parse(Console.ReadLine());

                tablero.TableroJuego[guardarFila, guardarColumna].moverPieza(tablero.TableroJuego,guardarFila,guardarColumna,jugador);

                vueltas++;
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
