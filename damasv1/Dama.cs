﻿using System;
using System.Collections.Generic;
using System.Text;

namespace damasv1
{
    class Dama
    {
        private bool estado;
        private int jugador;
        private string pieza;
        private bool promovida;
        
        public Dama() 
        {
            estado = false;
            jugador = 0;
            pieza = " ";
            promovida = false;
        }
        public Dama(int jugador)
        {
            this.jugador = jugador;
            estado = true;
            pieza = "O";
            promovida = false;
        }

        public bool Estado 
        {
            get { return estado; }
            set { estado = value; }
        }

        public int Jugador 
        {
            get { return jugador;}
            set { jugador = value; }
        }

        public string Pieza 
        {
            get { return pieza; }
            set { pieza = value; }
        }
        public Dama[,] moverPieza(Dama[,] tablero, int fila, int columna, int jugador)
        {
            if (promovida == true)
            {
                return tablero;
            }
            else
            {
                Console.WriteLine("Mover Izquierda o Derecha (0-1)");
                int dirrecion = int.Parse(Console.ReadLine());

                if (dirrecion == 0)
                {
                    if (columna == 0)
                    {
                        Console.WriteLine("Invalido");
                        return tablero;
                    }
                    else
                    {
                        if (jugador == 1)
                        {
                            if (tablero[fila - 1, columna - 1].Jugador == jugador)
                            {
                                Console.WriteLine("Tines una pieza tuya delante");
                                return tablero;
                            }
                            else
                            {
                                tablero[fila - 1, columna - 1] = tablero[fila, columna];
                                tablero[fila, columna] = new Dama();
                                return tablero;
                            }
                        }
                        else
                        {
                            if (tablero[fila + 1, columna - 1].Jugador == jugador)
                            {
                                Console.WriteLine("Tines una pieza tuya delante");
                                return tablero;
                            }
                            else
                            {
                                tablero[fila + 1, columna - 1] = tablero[fila, columna];
                                tablero[fila, columna] = new Dama();
                                return tablero;
                            }
                        }
                    }
                }
                else
                {
                    if (columna == 7)
                    {
                        Console.WriteLine("Invalido");
                        return tablero;
                    }
                    else
                    {
                        if (jugador == 1)
                        {
                            int guardarJugador = tablero[fila - 1, columna + 1].Jugador;
                            if (guardarJugador == jugador)
                            {
                                Console.WriteLine("Tines una pieza tuya delante");
                                return tablero;
                            }
                            else
                            {
                                tablero[fila - 1, columna + 1] = tablero[fila, columna];
                                tablero[fila, columna] = new Dama();
                                return tablero;
                            }
                        }
                        else
                        {
                            if (tablero[fila + 1, columna + 1].Jugador == jugador)
                            {
                                Console.WriteLine("Tines una pieza tuya delante");
                                return tablero;
                            }
                            else
                            {
                                tablero[fila + 1, columna + 1] = tablero[fila, columna];
                                tablero[fila, columna] = new Dama();
                                return tablero;
                            }
                        }
                    }
                }
            }
        }
        public void comerPieza() 
        {
        }


        
    }
}
