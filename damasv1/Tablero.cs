﻿using System;
using System.Collections.Generic;
using System.Text;

namespace damasv1
{
    class Tablero
    {
        private Dama[,] tableroJuego;

        public Tablero() 
        { 
        }

        public void crearTablero() 
        {
            tableroJuego = new Dama[8,8];
            for(int i = 0; i < 8; i++) 
            {
                for(int j = 0; j < 8; j++) 
                {
                    if(i == 3 || i == 4) 
                    {
                        tableroJuego[i, j] = new Dama();
                    }
                    else 
                    {
                        bool iEsPar;
                        bool jEsPar;

                        if ((i % 2) == 0)
                        {
                            iEsPar = true;
                        }
                        else
                        {
                            iEsPar = false;
                        }
                        if ((j % 2) == 0)
                        {
                            jEsPar = true;
                        }
                        else
                        {
                            jEsPar = false;
                        }

                        if (i == 0 || i == 1 || i == 2)
                        {
                            if (iEsPar == true && jEsPar == true)
                            {
                                tableroJuego[i, j] = new Dama(2);
                            }
                            else if (iEsPar == false && jEsPar == false)
                            {
                                tableroJuego[i, j] = new Dama(2);
                            }
                            else
                            {
                                tableroJuego[i, j] = new Dama();
                            }
                        }
                        else
                        {
                            if (iEsPar == true && jEsPar == true)
                            {
                                tableroJuego[i, j] = new Dama(1);
                            }
                            else if (iEsPar == false && jEsPar == false)
                            {
                                tableroJuego[i, j] = new Dama(1);
                            }
                            else
                            {
                                tableroJuego[i, j] = new Dama();
                            }
                        }
                    }
                }
            }
        }

        public Dama[,] TableroJuego 
        {
            get { return tableroJuego; }
            set { tableroJuego = value; }
        }
    }
}
