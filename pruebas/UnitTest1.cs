using Microsoft.VisualStudio.TestTools.UnitTesting;
using damasv1;
namespace pruebas
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void tamañoDeTablero()
        {
            Dama[,] tableroPruebaLogintud = new Dama[8, 8];
            Tablero tableroNormal = new Tablero();

            tableroNormal.crearTablero();

            int tamañoDetableroPruebas1 = tableroPruebaLogintud.GetLength(0);
            int tamañoDetableroPruebas2 = tableroPruebaLogintud.GetLength(1);

            int tamañoDeTableroNormal1 = tableroNormal.TableroJuego.GetLength(0);
            int tamañoDeTableroNormal2 = tableroNormal.TableroJuego.GetLength(1);

            Assert.AreEqual(tamañoDetableroPruebas1, tamañoDeTableroNormal1);
            Assert.AreEqual(tamañoDetableroPruebas2, tamañoDeTableroNormal2);
        }
        
        [TestMethod]
        public void crearTableroDeDamas() 
        {
            Tablero tableronormal = new Tablero();
            Dama[,] tableroPrueba;

            tableroPrueba = new Dama[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (i == 3 || i == 4)
                    {
                        tableroPrueba[i, j] = new Dama();
                    }
                    else
                    {
                        bool iEsPar;
                        bool jEsPar;

                        if ((i % 2) == 0)
                        {
                            iEsPar = true;
                        }
                        else
                        {
                            iEsPar = false;
                        }
                        if ((j % 2) == 0)
                        {
                            jEsPar = true;
                        }
                        else
                        {
                            jEsPar = false;
                        }

                        if (i == 0 || i == 1 || i == 2)
                        {
                            if (iEsPar == true && jEsPar == true)
                            {
                                tableroPrueba[i, j] = new Dama(2);
                            }
                            else if (iEsPar == false && jEsPar == false)
                            {
                                tableroPrueba[i, j] = new Dama(2);
                            }
                            else
                            {
                                tableroPrueba[i, j] = new Dama();
                            }
                        }
                        else
                        {
                            if (iEsPar == true && jEsPar == true)
                            {
                                tableroPrueba[i, j] = new Dama(1);
                            }
                            else if (iEsPar == false && jEsPar == false)
                            {
                                tableroPrueba[i, j] = new Dama(1);
                            }
                            else
                            {
                                tableroPrueba[i, j] = new Dama();
                            }
                        }
                    }
                }
            }

            tableronormal.crearTablero();
            int contadorIgualdad =0;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if(tableronormal.TableroJuego[i,j].Pieza == tableroPrueba[i, j].Pieza) 
                    {
                        contadorIgualdad++;
                    }
                }
            }

            Assert.AreEqual(64,contadorIgualdad);
        }

    }
}
